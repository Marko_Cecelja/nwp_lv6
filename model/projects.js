var mongoose = require('mongoose');
var projectSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number,
    jobsDone: String,
    startDateTime: { type: Date, default: Date.now },
    endDateTime: { type: Date, default: Date.now },
    members: [{
        firstName: String,
        lastName: String
    }]
});
mongoose.model('Project', projectSchema);